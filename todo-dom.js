// GLOBAL DATA
// let todos = [ 'Learn HTML', 'Learn CSS' ];
let todos = [ 
    { id: 1, title: 'Learn HTML', status: 'incomplete' },
    { id: 10, title: 'Learn CSS', status: 'complete' },
    { id: 23, title: 'Learn DOM', status: 'incomplete' }
]

// SELECT DOM
const todoList = document.querySelector('.todo-list');
const todoButton = document.querySelector('.todo-button');
const todoInput = document.querySelector('.todo-input');

// EVENT LISTENERS
document.addEventListener('DOMContentLoaded', getTodoList);
todoButton.addEventListener('click', save);

function getTodoList() {
    todos.map(todo => {
        // { id: 1, title: 'Learn HTML', status: 'incomplete' }
        createListTodo(todo['title'])
    })
}

function save(event) {
    // id nya harus bertambah sesuai dengan id terakhir yang ada pada data
    // statusnya default incomplete

    event.preventDefault();
    let newTodo = { id: todos[todos.length - 1]['id'] + 1, title: todoInput.value, status: 'incomplete' }

    todos.push(newTodo);

    createListTodo(todoInput.value);
    todoInput.value = '';
}

function createListTodo(todo) {
    const newTodo = document.createElement('li');
    newTodo.innerText = todo;

    todoList.appendChild(newTodo);
}

// function getTodoList() {
//     todos.map(todo => {
//         createListTodo(todo);
//     })
// }

// function save(event) {
//     event.preventDefault(); 
//     todos.push(todoInput.value);

//     createListTodo(todoInput.value);
//     todoInput.value = '';
// }

// function createListTodo(todo) {
//     const newTodo = document.createElement('li');
//     newTodo.innerText = todo;

//     todoList.appendChild(newTodo);
// }


// ============= SANDBOX PENJELASAAN MATERI SLIDE =============

// let numbers = [];

// const app = document.querySelector("#root");
// const box = document.querySelector(".box");

// console.log(box);

// // membuat element html
// const greetings = document.createElement('h1');
// // greetings.innerHTML = "<bold>Hi</bold>"
// greetings.innerText = "Hello World";

// const cover = document.createElement('img');
// cover.setAttribute('src', 'https://images-na.ssl-images-amazon.com/images/I/51zkTXeUIwL._SX338_BO1,204,203,200_.jpg')

// const buttonAlert = document.createElement('button');
// const textButton = document.createTextNode("Click me");
// buttonAlert.appendChild(textButton);

// buttonAlert.addEventListener('click', function() {
//     numbers.push(Math.floor(Math.random() * 5));
// })

// app.appendChild(buttonAlert)
// app.appendChild(cover);
// app.appendChild(greetings);

// console.log(app);
// console.log(greetings);