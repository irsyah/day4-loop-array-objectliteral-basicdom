# TUGAS

## No. 1
buat function dengan nama ularTangga menerima parameter number.
Output dari function ini adalah array multidimensi

```Javascript
function ularTangga(number) {

}

console.log(ularTangga(4));
// [
//     [ 1, 2, 3, 4 ],
//     [ 8, 7, 6, 5 ],
//     [ 9, 10, 11, 12 ],
//     [ 16, 15, 14, 13 ]
// ]
```

## No. 2
Buatlah function totalBalanceDriver yang menerima parameter array of object.
- Perhitungan rating didapatkan dari rata-rata rating customer driver tersebut
- Perhitungan Balance didapatkan dari 30% dari total pembayaran customer

```JavaScript
function totalBalanceDriver(drivers) {

}

console.log(totalBalanceDriver([
    { nama: 'Ali', ratings: [ { customer: 'Bella', rating: 4, totalPembayaran: 10000 }, { customer: 'Cinta', rating: 5, totalPembayaran: 15000 } ] },
    { nama: 'Zaki', ratings: [ { customer: 'Erika', rating: 4, totalPembayaran: 20000 }, {customer: 'Doni', rating: 3.5, totalPembayaran: 45000 }, {customer: 'Fadli', rating: 3, totalPembayaran: 5000 } ] }
]))

// [
//     { nama: 'Ali', rating: 4.5, balance: 6500 },
//     { nama: 'Zaki', rating: 3.5, balance: 21000 }
// ]

```

## No. 3
Diberikan data array of object sebagai berikut:

```JavaScript
let books = [
    { 
        id: 1,
        title: `Harry Potter and the Sorcerer's Stone`, 
        desc: `Harry Potter has no idea how famous he is. That's because he's being raised by his miserable aunt and uncle who are terrified Harry will learn that he's really a wizard, just as his parents were. But everything changes when Harry is summoned to attend an infamous school for wizards, and he begins to discover some clues about his illustrious birthright. From the surprising way he is greeted by a lovable giant, to the unique curriculum and colorful faculty at his unusual school, Harry finds himself drawn deep inside a mystical world he never knew existed and closer to his own noble destiny.`, 
        coverImage: 'https://images-na.ssl-images-amazon.com/images/I/51HSkTKlauL._SX346_BO1,204,203,200_.jpg', 
        stocks: 3,
        author: `J.K Rowling`
    },
    { 
        id: 2
        title: `Artemis Fowl: The Arctic Incident`, 
        desc: `Artemis Fowl receives an urgent e-mail from Russia. In it is a plea from a man who has been kidnapped by the Russian Mafiya: his father. As Artemis rushes to his rescue, he is stopped by a familiar nemesis, Captain Holly Short of the LEPrecon Unit. Now, instead of battling the fairies, Artemis must join forces with them if he wants to save one of the few people in the world he loves.`, 
        coverImage: 'https://images-na.ssl-images-amazon.com/images/I/51zkTXeUIwL._SX338_BO1,204,203,200_.jpg', 
        stocks: 1
        author: `Eoin Colfer`
    }
]
```

Kamu diminta untuk membuat HTML, CSS dan DOM untuk menampilkan data tersebut.

1. Dapat menambahkan data book yang ada
2. Tampilkan list data book tersebut yang menampilkan:
    - Cover Image
    - Title
    - Author
    - Desc
    - Stock
3. Pada setiap data book-nya ada tombol "BUY" dimana ketika menekan tombol "BUY" maka stock buku tersebut akan berkurang 
4. JIKA stock sudah 0, tombol "BUY" harus disable

UNTUK CSS BOLEH MENGGUNAKAN FRAMEWORK BOOTSTRAP/BULMA/TAILWIND DLL


BUATLAH PRIVATE REPOSITORY GITLAB DENGAN NAMA "book-store-namaKamu", ADD irsyah SEBAGAI MEMBER DENGAN ROLE DEVELOPERS AKSES SAMPAI 1 NOVEMBER