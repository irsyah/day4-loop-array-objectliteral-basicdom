// // WHILE
// let number = 0; // 4

// while (number < 4) { //  < 4
//     console.log("Halo");
//     number++;
//     // number = number + 1
//     // number += 1; 
// }

// // FOR
// for (let i = 0; i < 4; i++) {
//     console.log('Hi');
// }

// // DO WHILE
// let index = 0
// do {
//   //pasti akan berjalan at least 1 kali 
//   console.log('Hola');
// } while (index > 1) 


// // ARRAY
// // let names = new Array();
// let names = [];

// // assign value pada array 
// //1. cara manual
// names[0] = 'Rofiq';
// names[1] = 'Ilham';
// names[names.length] = 'Tama';
// names[names.length] = 'Orlynz';

// //2. menggunakan built in function dari Array
// names.push('Azizi');
// names.push('Mala');
// names.push('Faza');
// names.push('Helmi', 'Fauzan', 'Icha');

// let hasilPop = names.pop();
// let hasilShift = names.shift();

// names.unshift('Rhaya');

// console.log(names);
// console.log(names.length);
// console.log(`Data yang di pop: ${hasilPop}`);
// console.log(`Data yang di shift: ${hasilShift}`);

// const animals = ['ant', 'bison', 'camel', 'duck', 'elephant'];

// let hasilSlice = animals.slice(1, 4); // [bison, camel, duck]
// console.log(hasilSlice);

// for (let i = 0; i < names.length; i++) {
//     console.log(`i: ${i} -> ${names[i]}`);
// }

// names.map(function(name, index) {
//     console.log(`index ${index} berisi ${name}`);
// })

// //SAMA SAJA DENGAN
// names.map((name, index) => {
//     console.log(`${name} di index ${index}`);
// })

// let genders = [ 'male', 'female', 'female', 'famale', 'male', 'male' ]; // [ 'Mr', 'Miss', 'Miss', 'Mr', 'Mr' ]

// let hasilGenders = genders.map((gender) => {
//     if (gender === 'female') {
//         return 'Miss';
//     } else if (gender === 'male') {
//         return 'Mr'
//     }
// })

// console.log(hasilGenders);

// // MULTIDIMENSIONAL ARRAY
// console.log("===========MULTIDIMENSIONAL ARRAY=========");
// let students = [ 'Nafi', 'Ilham', 'Mala' ];
// let kelas = [];

// kelas.push(students);
// kelas.push('G2Academy');
// kelas.push([1, 2, 3]);
// console.log(kelas); // [ [ 'Nafi', 'Ilham', 'Mala' ], 'G2Academy',  ]

// for (let i = 0; i < kelas.length; i++) {
//     if (Array.isArray(kelas[i])) {
//         for (let j = 0; j < kelas[i].length; j++) {
//             console.log(kelas[i][j]);
//         }
//     } else {
//         console.log(kelas[i]);
//     }
// }

// // console.log(kelas[0][1]);

// kelas.map((klas) => {
//     if (Array.isArray(klas)) {
//         klas.map(el => {
//             console.log(el);
//         })
//     } else {
//         console.log(klas);
//     }
// })

// // OBJECT LITERAL
// console.log("==============OBJECT LITERAL================");
// // let person = {}; // inisialisasi 

// // 3 cara untuk mengisi data pada object
// // 1. direct to object 
// // let namaVariable = { key: value }
// let person = { firstName: 'Irsyah'}


// // 2. using dot (.)
// // namaVariable.key = value;
// let bootcamp = {};
// bootcamp.nama = 'G2Academy';

// person.last_Name = 'Mardiah';

// // 3. using square bracket
// // namaVariable['key'] = value;
// person['email'] = 'irsyah@g2academy.co';

// let keys = [ 'nama', 'email', 'gender' ];
// let kataKunci = 'jenisKelamin'

// console.log(keys[2]); //gender

// person[keys[2]] = 'female';
// person[kataKunci] = 'female';


// console.log(person);
// console.log(bootcamp);

// // CARA AKSES OBJECT LITERAL
// // 1. menggunakan dot (.)
// // namaVariable.key
// console.log(person.email);

// // 2. menggunakan square bracket ([])
// // namaVariable['key']
// console.log(person['firstName']);
// console.log(person[keys[2]]);


// // ARRAY OF OBJECT
// console.log("==========ARRAY OF OBJECT===========");
// let data = [
//     { product: 'Iphone 12', stocks: 3 },
//     { product: 'Samsung Flip', stocks: 5 },
//     { product: 'Macbook Pro M1', stocks: 10 }
// ];

// let totalStock = 0;

// for (let i = 0; i < data.length; i++) {
//     let product = data[i];

//     console.log(product.product);
//     console.log(product['stocks']);
//     totalStock = totalStock + product['stocks'];
// }

// // let totalStock = 0;
// // data.map(element => {
// //     totalStock = totalStock + element['stocks']
// // })

// console.log(totalStock);

// console.log("======= CONTOH DRIVER ========");


// function averageRatings(drivers) {
//     let result = {};

//     for (let i = 0; i < drivers.length; i++) {
//         let namaDriver = drivers[i]['nama'];
//         let ratings = drivers[i]['ratings'];
//         let totalRating = 0;
//         for (let iRat = 0; iRat < ratings.length; iRat++) {
//             totalRating = totalRating + ratings[iRat];
//         }
//         console.log(namaDriver);
//         let averageRating = totalRating/ratings.length;
//         result[namaDriver] = averageRating;

//         // drivers[i]['averageRating'] = averageRating;
        
//     }

//     // return result;
//     // return drivers
// }

// console.log(averageRatings([
//     { nama: 'Ali', ratings: [ 4, 3, 5, 3, 4 ] },
//     { nama: 'Zaki', ratings: [ 3, 4, 2 ] }
// ]));
// // {
// //     'Ali': 3.8,
// //     'Zaki': 3
// // }

// // [
// //     { nama: 'Ali', ratings: [ 4, 3, 5, 3, 4 ], averageRatings: 3.8 }
// //     { nama: 'Zaki', ratings: [ 3, 4, 2 ], averageRatings: 3 }
// // ]

function totalBalanceDriver(params) {
    let driver = [];
    for (let i = 0; i < params.length; i++) {
        var temp1 = [];
        temp1['nama'] = params[i]['nama'];
        var num = 0;
        var bal = 0;
        var etc = 0;

        for (let j = 0; j < params[i]['ratings'].length; j++) {
            num = num + params[i]['ratings'][j]['rating'];
            bal = bal + params[i]['ratings'][j]['totalPembayaran'];
            etc = j + 1;
        }
        num = num / etc;
        bal = bal * 0.3;
        temp1['rating'] = num;
        temp1['balance'] = bal;
        console.log(temp1);
        driver.push(temp1);
    }
    return driver;
}

console.log(totalBalanceDriver([
    { nama: 'Ali', ratings: [ { customer: 'Bella', rating: 4, totalPembayaran: 10000 }, { customer: 'Cinta', rating: 5, totalPembayaran: 15000 } ] },
    { nama: 'Zaki', ratings: [ { customer: 'Erika', rating: 4, totalPembayaran: 20000 }, {customer: 'Doni', rating: 3.5, totalPembayaran: 45000 }, {customer: 'Fadli', rating: 3, totalPembayaran: 5000 } ] }
]))
